package com.demo.entity;

import lombok.Data;

@Data
public class LoginTicket {

    /**
    用户id
     */
    private String userId;

    /**
    唯一标识
     */
    private String uuid;

    /**
    状态 0表示未扫码; 1表示扫码中; 2表示扫码成功
     */
    private int status;
}
