package com.demo.entity;

import lombok.Data;

@Data
public class User {

    /**
     * 用户Id
     */
    private String userId;

    /**
     * 姓名
     */
    private String userName;
}
