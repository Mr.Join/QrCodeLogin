package com.demo.service;

import com.demo.entity.User;
import com.demo.utils.CommonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private RedisTemplate redisTemplate;

    public User getLoggedUser(String userId) {
        String redisKey = CommonUtil.getUserKey(userId);
        return (User) redisTemplate.opsForValue().get(redisKey);
    }

    public void loginByPhone(User user) {
        String redisKey = CommonUtil.getUserKey(user.getUserId());
        redisTemplate.opsForValue().set(redisKey, user);
    }
}
