package com.demo.service;

import com.demo.entity.LoginTicket;
import com.demo.entity.User;
import com.demo.utils.CommonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Service
public class LoginService {

    private final int WAIT_EXPIRED_SECONDS = 60 * 5;

    private final int LOGIN_EXPIRED_SECONDS = 3600 * 24;

    @Autowired
    private RedisTemplate redisTemplate;

    public String createQrImg() {
        // 生成loginTicket
        String uuid = CommonUtil.generateUUID();
        LoginTicket loginTicket = new LoginTicket();
        loginTicket.setUuid(uuid);
        loginTicket.setStatus(0);

        // 存入redis
        String redisKey = CommonUtil.getTicketKey(loginTicket.getUuid());
        redisTemplate.opsForValue().set(redisKey, loginTicket, WAIT_EXPIRED_SECONDS, TimeUnit.SECONDS);

        return uuid;
    }

    public boolean scanQrCodeImg(String uuid, String userId) {
        String ticketKey = CommonUtil.getTicketKey(uuid);
        String userKey = CommonUtil.getUserKey(userId);
        LoginTicket loginTicket = (LoginTicket) redisTemplate.opsForValue().get(ticketKey);
        User user = (User) redisTemplate.opsForValue().get(userKey);
        if (user == null || loginTicket == null) {
            return false;
        } else {
            loginTicket.setStatus(1);
            redisTemplate.opsForValue().set(ticketKey, loginTicket, redisTemplate.getExpire(ticketKey, TimeUnit.SECONDS), TimeUnit.SECONDS);
        }
        return true;
    }

    public boolean confirmLogin(String uuid, String userId) {
        String redisKey = CommonUtil.getTicketKey(uuid);
        LoginTicket loginTicket = (LoginTicket) redisTemplate.opsForValue().get(redisKey);
        boolean logged = true;
        if (loginTicket == null) {
            logged = false;
        } else {
            loginTicket.setUserId(userId);
            loginTicket.setStatus(2);
            redisTemplate.opsForValue().set(redisKey, loginTicket, LOGIN_EXPIRED_SECONDS, TimeUnit.SECONDS);
        }
        return logged;
    }
}
