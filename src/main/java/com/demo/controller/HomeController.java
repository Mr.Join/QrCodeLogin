package com.demo.controller;

import com.demo.entity.Response;
import com.demo.entity.User;
import com.demo.service.LoginService;
import com.demo.service.UserService;
import com.demo.utils.TokenUtil;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@Controller
@Slf4j
public class HomeController {

    @Autowired
    LoginService loginService;

    @Autowired
    UserService userService;

    @RequestMapping(path = "/index", method = RequestMethod.GET)
    public String home() {
        return "index";
    }

    @RequestMapping(path = "/getUser", method = RequestMethod.GET)
    @ResponseBody
    public Response getUser(HttpServletRequest request) {
        String token = request.getHeader("access_token");
        if (token == null || !TokenUtil.verifyToken(token)) {
            return Response.createErrorResponse("token无效");
        }
        Claims claims = Jwts.parser()
                .setSigningKey(TokenUtil.SECRET)
                .parseClaimsJws(token)
                .getBody();
        String userId = (String) claims.get("userId");
        return Response.createResponse(null, userService.getLoggedUser(userId));
    }
}
