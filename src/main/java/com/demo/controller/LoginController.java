package com.demo.controller;

import cn.hutool.extra.qrcode.QrCodeUtil;
import com.alibaba.fastjson.JSONObject;
import com.demo.entity.LoginTicket;
import com.demo.entity.Response;
import com.demo.entity.User;
import com.demo.service.LoginService;
import com.demo.service.UserService;
import com.demo.utils.CommonUtil;
import com.demo.utils.TokenUtil;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/login")
public class LoginController {

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    LoginService loginService;

    @Autowired
    UserService userService;

    @Value("${login-url}")
    private String loginURL;

    @RequestMapping(path = "/getQrCodeImg", method = RequestMethod.GET)
    public String createQrCodeImg(Model model) {

        String uuid = loginService.createQrImg();
        String QrCode = Base64.encodeBase64String(QrCodeUtil.generatePng(loginURL + uuid, 300, 300));

        model.addAttribute("uuid", uuid);
        model.addAttribute("QrCode", QrCode);
        return "login";
    }

    @RequestMapping(path = "/getQrCodeState/{uuid}", method = RequestMethod.GET)
    @ResponseBody
    public Response getQrCodeState(@PathVariable("uuid") String uuid) throws InterruptedException {
        JSONObject data = new JSONObject();
        String redisKey = CommonUtil.getTicketKey(uuid);
        LoginTicket loginTicket = (LoginTicket) redisTemplate.opsForValue().get(redisKey);
        if (loginTicket == null) {
            data.put("status", -1);
            return Response.createResponse("二维码已过期!", data);
        }

        int status = loginTicket.getStatus();
        data.put("status", status);
        if (status == 2) {
            String userId = loginTicket.getUserId();
            User user = userService.getLoggedUser(userId);
            if (user != null) {
                String token = TokenUtil.buildToken(userId, user.getUserName());
                data.put("userId", userId);
                data.put("userName", user.getUserName());
                data.put("token", token);
                return Response.createResponse(null, data);
            }
            return Response.createErrorResponse("无用户信息!");
        }
        Thread.sleep(2000);
        String msg = status == 0 ? null : "已扫描, 等待确认";
        return Response.createResponse(msg, data);
    }

    @RequestMapping(path = "/scan/{uuid}/{userId}", method = RequestMethod.GET)
    public String scanQrCodeImg(Model model, @PathVariable("uuid") String uuid, @PathVariable("userId") String userId) {
        boolean scanned = loginService.scanQrCodeImg(uuid, userId);
        model.addAttribute("scanned", scanned);
        model.addAttribute("uuid", uuid);
        model.addAttribute("userId", userId);
        return "scan";
    }

    @RequestMapping(path = "/confirm/{uuid}/{userId}", method = RequestMethod.GET)
    @ResponseBody
    public Response confirmLogin(@PathVariable("uuid") String uuid, @PathVariable("userId") String userId) {
        boolean logged = loginService.confirmLogin(uuid, userId);
        String msg = logged ? "登录成功!" : "二维码已过期!";
        return Response.createResponse(msg, logged);
    }

    @RequestMapping(path = "/_phone", method = RequestMethod.POST)
    @ResponseBody
    public Response loginByPhone(@RequestBody User user) {
        userService.loginByPhone(user);
        return Response.createResponse("登录成功", user);
    }
}
