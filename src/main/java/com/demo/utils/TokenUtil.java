package com.demo.utils;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class TokenUtil {

    // 私钥
    public static final String SECRET = "ed640e3dba6f46df910fab3ca1ef2c2f";

    // token过期时间设置为24小时
    public static final int TOKEN_EXPIRE_TIME = 24 * 60 * 60 * 1000;

    public static String buildToken(String userId, String userName) {
        Map<String, Object> claims = new HashMap<>();
        // 附带userId, userName
        claims.put("userId", userId);
//        claims.put("userName", userName);
        return Jwts.builder()
                .setClaims(claims)
                .setExpiration(new Date(System.currentTimeMillis() + TOKEN_EXPIRE_TIME))
                .signWith(SignatureAlgorithm.HS512, SECRET)
                .compact();
    }

    public static boolean verifyToken(String token) {
        try {
            Jwts.parser()
                    .setSigningKey(SECRET)
                    .parseClaimsJws(token)
                    .getBody();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
